from django.conf.urls import url, include
from django.contrib import admin
from restapi.urls import urls as app_urls


urlpatterns = [
    url(r'^app/', include(app_urls, namespace='app')),
    url(r'^app/auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', admin.site.urls),
]
