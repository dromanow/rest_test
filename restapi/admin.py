from django.contrib import admin

from .models import Stat, Category, Item

admin.site.register(Stat)
admin.site.register(Category)
admin.site.register(Item)
