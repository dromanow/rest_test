from django.views.generic import TemplateView
from restapi.models import Stat, Category, Item
from rest_framework import serializers, viewsets, mixins


class LoggedInView(TemplateView):
    template_name = "logged_in.html"


class StatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stat


class StatViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Stat.objects.all()
    serializer_class = StatSerializer


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ItemSerializer(serializers.ModelSerializer, mixins.ListModelMixin):
    class Meta:
        model = Item


class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
