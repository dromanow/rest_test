from django.conf.urls import url, include
from rest_framework import routers

from .views import *

router = routers.DefaultRouter()
router.register(r'stat', StatViewSet)
router.register(r'category', CategoryViewSet)
router.register(r'item', ItemViewSet)

urls = [
    url(r'^logged_in/', LoggedInView.as_view()),
    url(r'^api/', include(router.urls)),
]
