from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


class Stat(models.Model):
    country = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    population = models.IntegerField()

    def __str__(self):
        return self.city


class Category(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Item(models.Model):
    name = models.CharField(max_length=200)
    categories = models.ManyToManyField(Category)
    value_int = models.IntegerField()
    value_float = models.FloatField()

    def __str__(self):
        return self.name


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
